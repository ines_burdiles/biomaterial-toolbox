# Biomaterial Toolbox
This is the documentation, description and reflections of Morgane and Inés' microchallenge 2 at MDEF Fabacademy 2021 where they developed a prototype of the Biomaterial Toolbox.

## What is the Biomaterial Toolbox?
The Biomaterial Toolbox, is a hybrid of carpentry toolbox, and laboratory, cooking and artistic equipment. All the things and tools we think we will need to guide biomaterial workshops with youth - or anyone new to the topic - during the third term of MDEF. The Biomaterial Toolbox is an artifact for dissemination of material literacy.

## Plan and Execution of the Project
The first aim was to create one artifact that contained and displayed the tools you need to make biomaterials, and also contains the casting molds for pouring biomaterials. We managed several ideas considering that we were to use to use cnc cutting for 2D pieces and 3D printing. First thing we made a cardboard prototype, where we arranged all the tools to be included in the kit, and get a general aidea of the total dimensions we needed. After that we started to build a 3d model in Rhinocerose to design with the real materials. The main issue of the Tollbox, is that it could easily become too heavy to be carried. We thought of only using 9mm plywood, and cutting all the parts to make a press-fit structure, and also a generative surface for the biomaterial pieces. Later we decided to cut in laser the casting molds and the pieces that would hold the jars and containers in place, out of 5mm acrylic. The 3D printing was used to make an ergonomic handle for the box.

![fabric](images/Zoom 1.JPG)
![fabric](images/IMG_2927.jpg)

## Iteration Process
- First Prototype : Cardboard prototype to give position and dimension considering all the objects that the toolbox would conatin.
- Second Iteration : 3D modelling of the cardboard prototype in real dimensions and widths of materials.
- Third Iteration : 3D modelling of the design solutions to make the toolbox operable as a storage and portable device at the same time.
- Fourth Iteration : Redefining the handle, instead of pocketing through the plywood structure, considering a solid wooden 1,8mm batten.
- Fifth Iteration : Design of specific acrylic pieces.
- Sixth Iteration : Handmodelling directly by squishing terracota clay, which was later 3d scanned, to be 3d printed and replicated to have the solid wooden batten inside.

![fabric](images/IMG_2946.jpg)
![fabric](images/IMG_2944.jpg)
![fabric](images/Rhino 2.JPG)
![fabric](images/Rhino 1.JPG)
![fabric](images/Rhino 3.JPG)

## What materials do you need?
- A 1310 x 940 mm board of 9mm birch plywood or any kind of wood board you have in hand.
- A 470 x 650 mm board of 5,0mm acrylic, we used transparent, but it can be any color you have in hand.
- A piece of clay, Skanect software to 3d scan, and a 3d printer to replicate.
- For a batch of biomaterial pieces: 200ml of water, 40gr of gelatine, and any natural dye, such as potato boiled water, or the red water from boiling avocado pits, which is what was used in this version. You can also add some drops of essential oil to avoid mold and have a nice smell.

## Step by Step fabrication of CNC Cut pieces
- Measure the average width of your board with a precision scale. Our's was 9,24mm
- Do the press fit tests, that consists more less into adding 0,2mm to the female, and subtract 0,2mm from the male.
- End mill: 6mm, 1 flute, flat.
- Cut direction : climb (down cut)
- Speed : 18.000 RPM (search in manufacturer of the end mill)
- Plunge: 3500
- Cut : 4500
- Retract : 4500
- Clearance Plane : 20 on Z

![fabric](images/IMG_2976.jpg)
![fabric](images/IMG_2984.jpg)
![fabric](images/IMG_3063.PNG)
![fabric](images/IMG_3064.PNG)
![fabric](images/IMG_3017.jpg)
![fabric](images/IMG_3067.PNG)
![fabric](images/IMG_3033.jpg)
![fabric](images/IMG_3065.PNG)
![fabric](images/2021-03-19 12.21.36.jpg)

## Future Developments
The possible future developments for the project are to use the toolbox as a material kit for the workshops we will be conducting during the third term of the master.

## What you will find in this repo
- 3D Rhino model files
- Laser cut files, of the stencils (see details above)
- CNC cut files, of the box (see details above)
- Press fit tolerance test file (for box)
- Images of the process

## Downloads
- biomaterial_toolbox_for_cnc.nc
- Box_and_Pieces.3dm
- cnc.3dm
- rhinocam_biomaterial_toolbox_.3dm
- screws_biomaterial_toolbox_for_cnc.nc
- stencils_and_frames_toolbox.3dm
- toolbox_test.3dm

![fabric](images/2021-03-19 12.28.52.jpg)
![fabric](images/2021-03-19 13.26.14.jpg)
![fabric](images/2021-03-19 13.27.14.jpg)
![fabric](images/2021-03-19 13.27.07.jpg)
![fabric](images/2021-03-19 13.26.39.jpg)

## Links to personal Posts
- Morgane Sha'ban : https:///Users/MorganeShaban/Desktop/mdef-website/mdef/challenge02.html
- Inés Burdiles : https://ines_burdiles.gitlab.io/sitio/single-fabweek8.html
